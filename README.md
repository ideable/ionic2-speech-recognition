Ionic 2 Speech Recognition
=====================

This is a sample app with Speech recognition using webkit API in a Ionic 2 starter menu app.

## Using this project

You'll need the Ionic CLI with support for v2 apps:

```bash
$ npm install -g ionic
```

Then run:

```bash
$ ionic start myApp
```

More info on this can be found on the Ionic [Getting Started](http://ionicframework.com/docs/v2/getting-started/) page.
